package kbtg.tech.fx.websocket;

import kbtg.tech.fx.config.WebSocketSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class WebSocketCaller {
    @Autowired
    private Environment environment;

    public boolean connectAndSend()throws InterruptedException,ExecutionException{
        //"ws://localhost:8086/rate-websocket"
        String endpoint = environment.getProperty("websocket.url.endpoint");
        return connect(endpoint);
    }
    public boolean connectAndSend(String endpoint)throws InterruptedException,ExecutionException{
        return connect(endpoint);
    }

    public boolean connect(String endpoint){
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(simpleWebSocketClient));
        SockJsClient sockJsClient = new SockJsClient(transports);
        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        //connect
        StompSessionHandler sessionHandler = new WebSocketSessionHandler("API Caller");
        try {
            StompSession session = stompClient.connect(endpoint, sessionHandler).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return true;
    }

}
