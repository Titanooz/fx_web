package kbtg.tech.fx.dao;

import kbtg.tech.fx.model.Currency;
import kbtg.tech.fx.model.CustomerRate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRateDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL_SELECT_RATE = "SELECT FCD.BUYING_BN,FCD.SELLING_BN,FCD.CURRENCY_PAIR_NAME FROM FXBOOTH.CUSTOMER_RATE_DETAIL FCD " +
            " LEFT JOIN FXBOOTH.CUSTOMER_RATE FCR ON FCD.CUSTOMER_RATE_CODE = FCR.CODE " +
            " LEFT JOIN FXBOOTH.BOOTH FB ON FCR.CODE = FB.CUSTOMER_RATE_CODE " +
            " WHERE FB.CODE = ? ";
    public List<Currency> listCurrencyByBoothCode(String code){
        return jdbcTemplate.query(SQL_SELECT_RATE, new Object[]{code}, new RowMapper<Currency>() {
            @Override
            public Currency mapRow(ResultSet resultSet, int i) throws SQLException {
                Currency currency = new Currency();
                currency.setBuying(resultSet.getString("BUYING_BN"));
                currency.setSelling(resultSet.getString("SELLING_BN"));
                currency.setCur(resultSet.getString("CURRENCY_PAIR_NAME"));
                return currency;
            }
        });
    }
    private static final String SQL_SELECT_LASTUPDATE = "SELECT VARCHAR_FORMAT(FCR.LAST_UPDATE, 'DD Mon YYYY') AS DATE,VARCHAR_FORMAT(FCR.LAST_UPDATE, 'HH12:MI AM') AS TIME FROM FXBOOTH.CUSTOMER_RATE FCR " +
            " LEFT JOIN FXBOOTH.BOOTH FB ON FCR.CODE = FB.CUSTOMER_RATE_CODE " +
            " WHERE FB.CODE = ? ";
    public CustomerRate getLastUpdate(String code){
        return jdbcTemplate.queryForObject(SQL_SELECT_LASTUPDATE, new Object[]{code}, new RowMapper<CustomerRate>() {
            @Override
            public CustomerRate mapRow(ResultSet resultSet, int i) throws SQLException {
                CustomerRate customerRate = new CustomerRate();
                customerRate.setDate(resultSet.getString("DATE"));
                customerRate.setTime(resultSet.getString("TIME"));
                return customerRate;
            }
        });
    }
    public List<Currency> listCurrencyCustomerPin(List<String> lsCode,String booth){
        List<String> unionAll = new ArrayList<>();
        List<String> params = new ArrayList<>();
        for(String currency : lsCode){
            StringBuffer sql = new StringBuffer(SQL_SELECT_RATE);
            sql.append(" AND CURRENCY_PAIR_NAME = ? ");
            unionAll.add(sql.toString());
            params.add(booth);
            params.add(currency);
        }
        String stringSQL = StringUtils.join(unionAll," UNION ALL ");
        return jdbcTemplate.query(stringSQL, params.toArray(), new RowMapper<Currency>() {
            @Override
            public Currency mapRow(ResultSet resultSet, int i) throws SQLException {
                Currency currency = new Currency();
                currency.setBuying(resultSet.getString("BUYING_BN"));
                currency.setSelling(resultSet.getString("SELLING_BN"));
                currency.setCur(resultSet.getString("CURRENCY_PAIR_NAME"));
                return currency;
            }
        });
    }
    public List<Currency> listAllCurrencyByBoothCode(List<String> lsCode,String booth){
        List<Object> params = new ArrayList<>();
        StringBuffer sql = new StringBuffer(SQL_SELECT_RATE);
        params.add(booth);
        if(lsCode.size() > 0){
            for(String  currency : lsCode){
                sql.append( " AND FCD.CURRENCY_PAIR_NAME <> ? " );
                params.add(currency);
            }
        }
        return jdbcTemplate.query(sql.toString(), params.toArray(), new RowMapper<Currency>() {
            @Override
            public Currency mapRow(ResultSet resultSet, int i) throws SQLException {
                Currency currency = new Currency();
                currency.setBuying(resultSet.getString("BUYING_BN"));
                currency.setSelling(resultSet.getString("SELLING_BN"));
                currency.setCur(resultSet.getString("CURRENCY_PAIR_NAME"));
                return currency;
            }
        });
    }
}
