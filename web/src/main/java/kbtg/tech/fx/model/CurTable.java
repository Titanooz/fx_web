package kbtg.tech.fx.model;

public class CurTable {

    private String htmlTable;
    private String maxPage;
    private String date;
    private String time;

    public CurTable(String htmlTable, String maxPage,String date,String time) {
        this.htmlTable = htmlTable;
        this.maxPage = maxPage;
        this.date = date;
        this.time = time;
    }

    public String getHtmlTable() {
        return htmlTable;
    }

    public void setHtmlTable(String htmlTable) {
        this.htmlTable = htmlTable;
    }

    public String getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(String maxPage) {
        this.maxPage = maxPage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
