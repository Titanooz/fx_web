package kbtg.tech.fx.config;

import kbtg.tech.fx.model.HelloMessage;
import org.springframework.messaging.simp.stomp.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class WebSocketSessionHandler extends StompSessionHandlerAdapter {

    private String userId;
    public WebSocketSessionHandler(String userId){
        this.userId = userId;
    }
    private void showHeaders(StompHeaders headers)
    {
        for (Map.Entry<String,List<String>> e:headers.entrySet()) {
            System.out.print("  " + e.getKey() + ": ");
            boolean first = true;
            for (String v : e.getValue()) {
                if ( ! first ) System.out.print(", ");
                System.out.print(v);
                first = false;
            }
            System.out.println();
        }
    }

    private void sendJsonMessage(StompSession session)
    {
        HelloMessage msg = new HelloMessage(userId);
        session.send("/app/hello", msg);
    }

    private void subscribeTopic(String topic,StompSession session)
    {
        session.subscribe(topic, new StompFrameHandler() {

            @Override
            public Type getPayloadType(StompHeaders headers) {
                return HelloMessage.class;
            }

            @Override
            public void handleFrame(StompHeaders headers,
                                    Object payload)
            {
                System.out.println(payload.toString());
            }
        });
    }

    @Override
    public void afterConnected(StompSession session,
                               StompHeaders connectedHeaders)
    {
        System.out.println("Connected! Headers:");
        showHeaders(connectedHeaders);

        subscribeTopic("/topic/greetings", session);
        sendJsonMessage(session);
    }
}
