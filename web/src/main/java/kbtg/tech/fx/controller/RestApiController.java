package kbtg.tech.fx.controller;

import kbtg.tech.fx.dao.CustomerRateDAO;
import kbtg.tech.fx.model.CurTable;
import kbtg.tech.fx.model.Currency;
import kbtg.tech.fx.model.CustomerRate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api")
public class RestApiController {
    @Autowired
    private CustomerRateDAO customerRateDAO;

    @RequestMapping(value = "/curs", method = POST)
    public List<Currency> getListCurs() throws Exception{
        List<Currency> listCurs = new ArrayList<>();
        listCurs.add(new Currency("USD","34.29","34.90"));
        listCurs.add(new Currency("EUR","45.75525","46.8225"));
        return listCurs;
    }

    @RequestMapping(value = "/cursTable", method = POST)
    public CurTable getCursTable(String code,String rows,String pins,HttpSession session) throws Exception{
//        int maxRow = 10;
        int maxRow = Integer.parseInt(rows);
        List<String> pinCurs = classifyPinCurs(pins);
        List<Currency> listCurs = customerRateDAO.listAllCurrencyByBoothCode(pinCurs,code);
        List<Currency> listPinCurs = customerRateDAO.listCurrencyCustomerPin(pinCurs,code);
        CustomerRate customerRate = customerRateDAO.getLastUpdate(code);
        int maxUnpinRow = maxRow;
        int maxPage = 0;
        StringBuilder strCursTable = new StringBuilder();
        if(listPinCurs.size()>0){
            int countCur = 0;
            maxPage = 1;
            maxUnpinRow = maxRow - listPinCurs.size();
            for(Currency cur:listPinCurs){
                countCur++;
                strCursTable.append("<tr class=\"pin\">");
                strCursTable.append("<td class=\"col-sm-1 font-table-td\" style=\"vertical-align:middle; border-right:0px; padding-right:0px;\"><span class=\"flag flag-"+setClassForRenderImg(cur.getCur())+"\" style=\"vertical-align:middle\"></span></td>");
                strCursTable.append("<td class=\"col-sm-1 font-table-td td-cur\" style=\"vertical-align:middle; border-left:0px; padding-left:0px; text-align:left\"><span>" + cur.getCur() + "</span></td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-buy\" style=\"vertical-align:middle;\">" + cur.getBuying() + "</td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-sell\" style=\"vertical-align:middle;\">" + cur.getSelling() + "</td>");
                strCursTable.append("</tr>");
                if(countCur>=maxRow){
                    break;
                }
            }
        }
        if(maxUnpinRow>0){
            maxPage = (int)Math.ceil((double)listCurs.size()/(double)maxUnpinRow);
            int countPage = 1;
            int countCur = 0;
            for(Currency cur:listCurs){
                countCur++;
                strCursTable.append("<tr class=\"page-"+countPage+"\">");
                strCursTable.append("<td class=\"col-sm-1 font-table-td\" style=\"vertical-align:middle; border-right:0px; padding-right:0px;\"><span class=\"flag flag-"+setClassForRenderImg(cur.getCur())+"\" style=\"vertical-align:middle\"></span></td>");
                strCursTable.append("<td class=\"col-sm-1 font-table-td td-cur\" style=\"vertical-align:middle; border-left:0px; padding-left:0px; text-align:left\"><span>" + cur.getCur() + "</span></td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-buy\" style=\"vertical-align:middle;\">" + cur.getBuying() + "</td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-sell\" style=\"vertical-align:middle;\">" + cur.getSelling() + "</td>");
                strCursTable.append("</tr>");
                if(countCur==maxUnpinRow){
                    countPage++;
                    countCur = 0;
                }
            }
            for(int i=listCurs.size();i<(maxUnpinRow*maxPage);i++){
                strCursTable.append("<tr class=\"page-"+countPage+" empty-tr\">");
                strCursTable.append("<td class=\"col-sm-1 font-table-td\" style=\"vertical-align:middle; border-right:0px;\"> </td>");
                strCursTable.append("<td class=\"col-sm-1 font-table-td td-cur\" style=\"vertical-align:middle; border-left:0px;\"> </td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-buy\" style=\"vertical-align:middle;\"> </td>");
                strCursTable.append("<td class=\"col-sm-5 font-table-td td-sell\" style=\"vertical-align:middle;\"> </td>");
                strCursTable.append("</tr>");
            }
        }
        return new CurTable(strCursTable.toString(),""+maxPage,customerRate.getDate(),customerRate.getTime());
    }
    private String setClassForRenderImg(String currency){
        return ((StringUtils.isNoneBlank(currency) && currency.length() > 2) ? currency.substring(0,2).toLowerCase():currency);
    }
    private List<String> classifyPinCurs(String curs){
        List<String> pinCurs = Arrays.asList(curs.split("\\|"));
        return pinCurs;
    }
}
