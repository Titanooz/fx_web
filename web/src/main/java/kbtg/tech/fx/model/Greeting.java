package kbtg.tech.fx.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Greeting {

    private String content;
    private String status;
    private String date;

    public Greeting() {
    }

    public Greeting(String content) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        this.date = sdf.format(new Date());
        this.content = content;
        this.status = "Reloading";
    }

    public String getContent() {
        return content;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }
}
