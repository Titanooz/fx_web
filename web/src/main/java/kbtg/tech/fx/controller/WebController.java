package kbtg.tech.fx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class WebController {

    @ResponseBody
    @RequestMapping(value = "/booth/{code}/{rows}/{pins}", method = GET)
    public ModelAndView home(@PathVariable String code, @PathVariable String rows, @PathVariable String pins, HttpSession session) throws Exception{
        ModelAndView view = new ModelAndView("index");
        view.addObject("code",code);
        view.addObject("rows",rows);
        view.addObject("pins",pins);
        return view;
    }

}
