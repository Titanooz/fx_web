package kbtg.tech.fx.controller;

import kbtg.tech.fx.model.Greeting;
import kbtg.tech.fx.model.HelloMessage;
import kbtg.tech.fx.websocket.WebSocketCaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GreetingController {

    @Autowired
    private WebSocketCaller webSocketCaller;
    private SimpMessagingTemplate template;

    @Autowired
    public GreetingController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping({"/hello"})
    @SendTo({"/topic/greetings"})
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(200); // simulated delay
        return new Greeting("Reload");
//        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }
    @ResponseBody
    @RequestMapping(value="/connect/greeting", method= RequestMethod.GET)
    public ResponseEntity<Greeting> helloGreeting() throws Exception {
        if(webSocketCaller.connectAndSend())
            return new ResponseEntity<Greeting>(new Greeting("Service API Caller"), HttpStatus.OK);
        return new ResponseEntity<Greeting>(new Greeting("Service API Caller"), HttpStatus.BAD_REQUEST);
    }

}
