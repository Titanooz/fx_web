package kbtg.tech.fx.model;

public class Currency {

    private String cur;
    private String buying;
    private String selling;

    public Currency() {}
    public Currency(String cur, String buying, String selling) {
        this.cur = cur;
        this.buying = buying;
        this.selling = selling;
    }

    public String getCur() {
        return cur;
    }
    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getBuying() {
        return buying;
    }
    public void setBuying(String buying) {
        this.buying = buying;
    }

    public String getSelling() {
        return selling;
    }
    public void setSelling(String selling) {
        this.selling = selling;
    }

}
